(function () {

 var app = angular.module('backend'); // recoge un modulo ya creado

	app.config(['$routeProvider', // nombre real
	function($routeProvider)  // variable que utilizamos dentro de la funcion
	{
		$routeProvider.when('/families',
		{
			templateUrl: "js/templates/families/list.html",
			controller: 'familyGetAllController',
			controllerAs: 'familyGetAllCtrl'
		})
		.when('/families/new', {
			templateUrl: 'js/templates/families/new.html',
			controller: 'familyCreateController',
			controllerAs: 'familyCreateCtrl'
		})
		.when('/family/show/:id', {
			templateUrl: 'js/templates/families/show.html',
			controller: 'familyGetIdController',
			controllerAs: 'familyGetIdCtrl'
		})
		.when('/family/edit/:id', {
			templateUrl: 'js/templates/families/edit.html',
			controller: 'familyEditController',
			controllerAs: 'familyEditCtrl'
		});
		
	}]);



	app.controller('familyGetAllController', function (Family, $timeout)
    {
		familiesCtrl = this;
        familiesCtrl.itemsPerPage = 20;
        familiesCtrl.rowCollection = [];
        familiesCtrl.displayedCollection = [];
		familiesCtrl.isLoading = true;
		familiesCtrl.isDeleted = false;
		familiesCtrl.message = "Familia borrada correctamente";


		Family.get().$promise.then(
			function(data) // success
			{
				familiesCtrl.isLoading = false;
				if(data.families !== undefined)
			 		familiesCtrl.rowCollection = data.families;
                familiesCtrl.displayedCollection = [].concat(familiesCtrl.rowCollection);
			},
			function(data) //error
			{
				familiesCtrl.isLoading = false;
			});

		familiesCtrl.remove = function(rowFam,idFam)
		{
            var confirmation = confirm("Desea eliminar la familia " + rowFam.nombre);
            var index = familiesCtrl.rowCollection.indexOf(rowFam);
            if(confirmation === true)
            {
                Family.delete({id:idFam}).$promise.then(
                    function(data) //success
                    {
                        familiesCtrl.isDeleted = true;
                        if(index !== -1)
                        {
                            familiesCtrl.rowCollection.slice(index,1);
                        }
                        $timeout(function()
                        {
                            familiesCtrl.isDeleted = false;
                        },2000);
                    },
                    function(data) //error
                    {
                        familiesCtrl.isDeleted = false;
                    });
            }
		};
	});

	app.controller('familyGetIdController', function ($routeParams, Family)
    {
		familyCtrl = this;
		familyCtrl.title = "Familia: ";
		familyCtrl.name ="";
		familyCtrl.message = "Familia no encontrada";
		familyCtrl.idFamily = $routeParams.id;
		
		familyCtrl.family = 
		{
			nombre: "",
            categorias : ''
		};
		
		Family.get({id:familyCtrl.idFamily}).$promise.then(function(data)
		{
			familyCtrl.name = data.family.nombre;
            familyCtrl.title += data.family.nombre;
			familyCtrl.family.nombre = data.family.nombre;
            familyCtrl.family.categorias = data.family.categorias;
		});

	});
    app.controller('familyEditController',function(Family,$routeParams)
    {
        familyCtrl = this;
        familyCtrl.name ="";
        familyCtrl.message = "Familia editada correctamente";
        familyCtrl.idFamily = $routeParams.id;

        familyCtrl.family =
        {
            nombre: ""
        };

        Family.get({id:familyCtrl.idFamily}).$promise.then(function(data)
        {
            familyCtrl.name = data.family.nombre;
            familyCtrl.family.nombre = data.family.nombre;

        });


        familyCtrl.submit = function ()
        {
            Family.update({id:familyCtrl.idFamily}, familyCtrl.family).$promise.then(
                function(data) // success
                {
                    familyCtrl.showSucces = true;
                    familyCtrl.showError = false;
                },
                function(data) // error
                {
                    // droga para recoger el mensaje que hemos recibido
                    var key = data[Object.keys(data)[0]][Object.keys(data[Object.keys(data)[0]])][0];

                    familyCtrl.message = "Error al editar Familia: " + key;
                    familyCtrl.showSucces = false;
                    familyCtrl.showError = true;
                });
        };


    });
	app.controller('familyCreateController', function (Family, $timeout )
    {
		familyNewCrtl = this;
		familyNewCrtl.title = "Alta Familia";
		familyNewCrtl.message = "Familia guardada correctamente";
		familyNewCrtl.showSucces = false;
		familyNewCrtl.showError = false;
		familyNewCrtl.family = 
		{
			nombre: ""
		};
		familyNewCrtl.oriFamily = angular.copy(familyNewCrtl.family);

		familyNewCrtl.submit = function()
		{
			Family.save(familyNewCrtl.family).$promise.then(
			function(families) // success
			{
		
				familyNewCrtl.family =  angular.copy(familyNewCrtl.oriFamily);
				familyNewCrtl.showSucces = true;
				familyNewCrtl.showError = false;				
			}, 
			function(families) // error
			{
				var key = data[Object.keys(data)[0]][Object.keys(data[Object.keys(data)[0]])][0];
				familyNewCrtl.showSucces = false;
				familyNewCrtl.showError = true;	
				familyNewCrtl.message = "Error al guardar familia: " + key;	
				
			});

			$timeout(function()
			{
				familyNewCrtl.showSucces = false;
				familyNewCrtl.showError = false;				
			},800);
			
		};

	});


})();