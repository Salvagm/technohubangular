(function () { // sirve para encapsuarl el ambito
	var app = angular.module('backend'); // recoge un modulo ya creado

	app.config(['$routeProvider', // nombre real
	function($routeProvider)  // variable que utilizamos dentro de la funcion
	{
		$routeProvider.when('/products',
		{
			templateUrl: "js/templates/products/list.html",
			controller: 'productGetAllController',
			controllerAs: 'productGetAllCtrl'
		})
		.when('/products/new', {
			templateUrl: 'js/templates/products/new.html',
			controller: 'productCreateController',
			controllerAs: 'productCreateCtrl'
		})
		.when('/product/show/:id', {
			templateUrl: 'js/templates/products/show.html',
			controller: 'productGetIdController',
			controllerAs: 'productGetIdCtrl'
		})
		.when('/product/edit/:id', {
			templateUrl: 'js/templates/products/edit.html',
			controller: 'productEditController',
			controllerAs: 'productEditCtrl'
		});
		
	}]);

	app.controller('productGetAllController', function (Product, $timeout)
    {
		var productsCtrl = this;
		productsCtrl.isLoading = true;
		productsCtrl.isDeleted = false;
		productsCtrl.message = "Producto borrado correctamente";
        productsCtrl.itemPerPage = 20;
		productsCtrl.rowCollection = [];
        productsCtrl.displayedCollection = [];


		Product.get().$promise.then(
        function(data) // success
        {
            productsCtrl.isLoading = false;
            if(data.products !== undefined)
            {
                productsCtrl.rowCollection = data.products;
            }
            productsCtrl.displayedCollection = [].concat(productsCtrl.rowCollection);

        },
        function(data) //error
        {
            productsCtrl.isLoading = false;
        });


		productsCtrl.remove = function(rowProduct,idProduct)
		{
            var confirmation = confirm("Desea eliminar el producto " + rowProduct.nombre);
            var index = productsCtrl.rowCollection.indexOf(rowProduct);
            if(confirmation === true)
            {
                Product.delete({id:idProduct}).$promise.then(
                    function(data) //success
                    {
                        productsCtrl.isDeleted = true;
                        if (index !== -1) {
                            productsCtrl.rowCollection.splice(index, 1);
                        }

                        $timeout(function()
                        {
                            productsCtrl.isDeleted = false;
                        },2000);
                    },
                    function(data) //error
                    {
                        productsCtrl.isDeleted = false;
                    });
            }
		};

	});

	app.controller('productGetIdController', function($routeParams, $location, Product, copyProduct)
    {
		var productCtrl = this;
		productCtrl.title = "Producto: ";
		productCtrl.message = "Producto no encontrado";
		productCtrl.idProduct = $routeParams.id;
		productCtrl.name ="";
        productCtrl.iva = 0;
		productCtrl.product =
		{
			nombre: "",
			descripcion: "",
			ref: "",
			precioProveedor: '',
			pvp: '',
			stockMin: '',
			stock: '',
            supliers : [],
            categories : []
		};
		Product.get({id:productCtrl.idProduct}).$promise.then(function(data)
		{
            productCtrl.iva = data.iva[0].porcentaje;
			productCtrl.name = data.product.nombre;
			productCtrl.title += productCtrl.name;
			productCtrl.product.ref = data.product.ref;
			productCtrl.product.nombre = data.product.nombre;
			productCtrl.product.descripcion = data.product.descripcion;
			productCtrl.product.precioProveedor = data.product.precioProveedor;
			productCtrl.product.pvp = data.product.pvp;
			productCtrl.product.stockMin = data.product.stockMin;
			productCtrl.product.stock = data.product.stock;
            productCtrl.product.categories = data.product.categorias;
            productCtrl.product.supliers = data.product.proveedores;
            productCtrl.product.pvpIVA = Math.round(productCtrl.product.pvp * productCtrl.iva*1000)/1000;
            productCtrl.product.precioProveedorIVA = Math.round(productCtrl.product.precioProveedor * productCtrl.iva*1000)/1000 ;
		},
		function(data)
		{

		});
        productCtrl.sendInfo = function()
        {
            copyProduct.product = angular.copy(productCtrl.product);
            copyProduct.product.categories = [];
            copyProduct.product.supliers = [];
            angular.forEach(productCtrl.product.categories, function (value,key) {
                copyProduct.product.categories.push(value.id);
            });
            angular.forEach(productCtrl.product.supliers, function (value, key) {
                copyProduct.product.supliers.push(value.id);
            });
            $location.path("/products/new");
        };
	});

	app.controller('productEditController', function ($routeParams, Product, Category, Suplier, Utils)
    {
		var productCtrl = this;
		productCtrl.title = "Editar Producto";
		productCtrl.message = "Producto editado correctamente";
		productCtrl.name ="";
        productCtrl.categories = [];
        productCtrl.supliers = [];
        productCtrl.idProduct = $routeParams.id;
        productCtrl.iva = 1.21;
		productCtrl.product =
		{
			nombre: "",
			descripcion: "",
			ref: "",
			precioProveedor: 1,
			pvp: 1,
			stockMin: '',
			stock: '',
            categories : [],
            supliers : []
		};

        Product.get({id:productCtrl.idProduct}).$promise.then(
        function(data)
        {

            productCtrl.name = data.product.nombre;
            productCtrl.product.ref = data.product.ref;
            productCtrl.product.nombre = data.product.nombre;
            productCtrl.product.descripcion = data.product.descripcion;
            productCtrl.product.precioProveedor = data.product.precioProveedor;
            productCtrl.product.pvp = data.product.pvp;
            productCtrl.product.stockMin = data.product.stockMin;
            productCtrl.product.stock = data.product.stock;
            productCtrl.iva = data.iva[0].porcentaje;
            angular.forEach(data.product.categorias, function (value,key) {
                productCtrl.product.categories.push(value.id);
            });

            console.log(data);
            angular.forEach(data.product.proveedores, function(value,key)
            {
                productCtrl.product.supliers.push(value.id);
            });

            Category.get().$promise.then(
            function(data) // success
            {
                if(data.categories !== undefined)
                    productCtrl.categories =  Utils.checkListOfItems(data.categories,productCtrl.product.categories);
            },
            function(data) //error
            {

            });
            Suplier.get().$promise.then(
            function (data) {
                if(data.supliers !== undefined)
                    productCtrl.supliers = Utils.checkListOfItems(data.supliers,productCtrl.product.supliers);
            },
            function (data) {

            }

            )
        },
        function (data) {
            // error al recibir datos
        });



		productCtrl.submit = function ()
		{
            console.log(productCtrl.product);
			Product.update({id:productCtrl.idProduct}, productCtrl.product).$promise.then(
			function() // success
			{
				productCtrl.showSucces = true;
				productCtrl.showError = false;
			}, 
			function(data) // error
			{
                var key = "Operacion no permitida";
                if(data.status != 500)
                {
                    // droga para recoger el mensaje que hemos recibido
                    key = data[Object.keys(data)[0]][Object.keys(data[Object.keys(data)[0]])][0];

                }

				productCtrl.message = "Error al editar producto: " + key;	
				productCtrl.showSucces = false;
				productCtrl.showError = true;		
			});
		};

        productCtrl.storeCategory = function(category)
        {
            // _isSelect -> No contenido en modelo, atributo propio
            category._isSelect = !category._isSelect;

            if(category._isSelect)
                productCtrl.product.categories.push(category.id);
            else
                productCtrl.product.categories.splice(productCtrl.product.categories.indexOf(category.id),1);
        };

        productCtrl.storeSuplier = function (suplier)
        {
            suplier._isSelect = !suplier._isSelect;

            if(suplier._isSelect)
                productCtrl.product.supliers.push(suplier.id);
            else
                productCtrl.product.supliers.splice(productCtrl.product.supliers.indexOf(suplier.id),1);
        };

	});

	app.controller('productCreateController', function ($timeout ,Product, Category, Suplier, Iva, copyProduct, Utils)
    {
		var productNewCtrl = this;
		productNewCtrl.title = "Alta producto";
		productNewCtrl.message = "Producto guardado correctamente";
		productNewCtrl.showSucces = false;
		productNewCtrl.showError = false;
        productNewCtrl.categories = [];
        productNewCtrl.supliers = [];
        productNewCtrl.iva = [];
		productNewCtrl.product =
		{
			nombre: "",
			descripcion: "",
			ref: "",
			precioProveedor: '',
			pvp: '',
			stockMin: '',
			stock: '',
            supliers : [],
            categories : [],
            idCat : 0
		};
        productNewCtrl.oriProduct = angular.copy(productNewCtrl.product);

        if(copyProduct.product !== null)
        {
            productNewCtrl.product = angular.copy(copyProduct.product);
            copyProduct.product = null;
        }

        Category.get().$promise.then(
        function(data) // success
        {
            if(data.categories !== undefined)
                productNewCtrl.categories = Utils.checkListOfItems(data.categories,productNewCtrl.product.categories);
        },
        function(data) //error
        {

        });
        Suplier.get().$promise.then(
        function(data)
        {
            if(data.supliers !== undefined)
                productNewCtrl.supliers = Utils.checkListOfItems(data.supliers,productNewCtrl.product.supliers);
        },
        function (data) {

        });
        Iva.get().$promise.then(
        function (data) {
            if(data.iva !== undefined)
                productNewCtrl.iva = data.iva[0].porcentaje;
        },
        function () {

        });

		productNewCtrl.submit = function()
        {
            console.log(productNewCtrl.mySupliers);
            console.log(productNewCtrl.product);
            Product.save(productNewCtrl.product).$promise.then(
            function(data) // success
            {
                productNewCtrl.product =  angular.copy(productNewCtrl.oriProduct);
                angular.forEach(productNewCtrl.categories, function(value,key)
                {
                    value._isSelect = false;
                });
                angular.forEach(productNewCtrl.supliers, function (value,key) {
                    value._isSelect = false;
                });
                productNewCtrl.product.categories = [];
                productNewCtrl.product.supliers = [];
                productNewCtrl.message = "Producto guardado correctamente";
                productNewCtrl.showSucces = true;
                productNewCtrl.showError = false;
            },
            function(data) // error
            {
                var key = "Operacion no permitida";
                if(data.status != 500)
                {
                    var firstKey = Object.keys(data)[0];
                    key = data[firstKey][Object.keys(data[firstKey])][0];
                }
				productNewCtrl.showSucces = false;
				productNewCtrl.showError = true;
                productNewCtrl.message = "Error al guardar producto: ";
                if(key !== undefined)
                    productNewCtrl.message += key;

            });

            $timeout(function()
            {
                productNewCtrl.showSucces = false;
                productNewCtrl.showError = false;
            },3000);

        };

        productNewCtrl.storeCategory = function(category)
        {
            // _isSelect -> No contenido en modelo, atributo propio
            category._isSelect = !category._isSelect;

            if(category._isSelect)
                productNewCtrl.product.categories.push(category.id);
            else
                productNewCtrl.product.categories.splice(productNewCtrl.product.categories.indexOf(category.id),1);
        };
        productNewCtrl.storeSuplier = function (suplier)
        {
            suplier._isSelect = !suplier._isSelect;

            if(suplier._isSelect)
                productNewCtrl.product.supliers.push(suplier.id);
            else
                productNewCtrl.product.supliers.splice(productNewCtrl.product.supliers.indexOf(suplier.id),1);
        };

    });

})();