/**
 * Created by salva on 26/10/15.
 */

(function ()
{

    var app = angular.module('backend');

    app.config(['$routeProvider', // nombre real
        function($routeProvider)  // variable que utilizamos dentro de la funcion
        {
            $routeProvider.when('/clients',
                {
                    templateUrl: "js/templates/clients/list.html",
                    controller: 'clientGetAllController',
                    controllerAs: 'clientGetAllCtrl'
                })
                .when('/clients/new', {
                    templateUrl: 'js/templates/clients/new.html',
                    controller: 'clientCreateController',
                    controllerAs: 'clientCreateCtrl'
                })
                .when('/client/show/:id', {
                    templateUrl: 'js/templates/clients/show.html',
                    controller: 'clientGetIdController',
                    controllerAs: 'clientGetIdCtrl'
                })
                .when('/client/edit/:id', {
                    templateUrl: 'js/templates/clients/edit.html',
                    controller: 'clientEditController',
                    controllerAs: 'clientEditCtrl'
                });

        }]);


    app.controller('clientGetAllController', function (Person,Company, Client, $timeout)
    {
        var ctrl = this;

        ctrl.isLoading = false;
        ctrl.showLoading = false;
        ctrl.isDeleted = false;
        ctrl.showError = false;
        ctrl.message = "Cliente borrado correctamente";
        ctrl.itemPerPage = 20;
        ctrl.rowCollection = [];
        ctrl.cType = 'person';
        ctrl.displayedCollection = [];

        ctrl.loadClients = function(type) // Carga cliente dependiendo de la seleccion
        {
            ctrl.showLoading = true;
            ctrl.isLoading = true;
            console.log(type);
            Client.get({type:type}).$promise.then(
            function(data)
            {
                ctrl.isLoading = false;
                ctrl.rowCollection = data.clients;
                if(ctrl.rowCollection.length !== 0)
                    ctrl.displayedCollection = [].concat(ctrl.rowCollection);
                console.log(data);
            },
            function(data)
            {
                ctrl.showError = true;
                console.log(data);
                ctrl.message = "Error al cargar los datos de clientes: ";
                ctrl.message += data.data.msg;
                $timeout(function()
                {
                    ctrl.showError = false;
                },2000);
            });



        };

        ctrl.remove = function(row,idClient) // elimina cliente accediendo al servico Client
        {
            var confirmation = confirm("Desea eliminar el cliente " + row.nombre);
            var index = ctrl.rowCollection.indexOf(row);
            if(confirmation === true)
            {
                Client.delete({id:idClient}).$promise.then(
                function(data)
                {
                    ctrl.isDeleted = true;
                    ctrl.message = "Cliente borrado correctamente";
                    if(index !== -1)
                        ctrl.rowCollection.splice(index,1);
                },
                function(data)
                {
                    ctrl.message = "No se ha podido dar de baja el cliente";
                    ctrl.showError = true;
                    ctrl.isDeleted = false;
                });
                $timeout(function()
                {
                    ctrl.isDeleted = false;
                    ctrl.showError = false;
                },2000);
            }
        };

        ctrl.flushData = function()
        {
            ctrl.displayedCollection = [];
            ctrl.rowCollection = [];
        };


    });

    app.controller('clientGetIdController', function(Client, $timeout, $routeParams)
    {
        var ctrl = this;
        ctrl.clientType = "company";
        ctrl.idClient = $routeParams.id;
        ctrl.hasError = false;

        ctrl.client = {};

        Client.get({id:ctrl.idClient}).$promise.then(
        function(data)
        {
            ctrl.clientType = data.clientType;
            ctrl.client = data.client;
        },
        function(data)
        {
            ctrl.hasError = true;
            ctrl.msg = "Problema al cargar el cliente seleccionado";
        });

    });


    app.controller('clientCreateController', function(Person,Company,Client,$timeout)
    {
        var ctrl = this;
        ctrl.showSucces = false;
        ctrl.showError = false;
        ctrl.message = "Cliente dado de alta correctamente";
        ctrl.clientType = "person";
        ctrl.clientsTypes = ['company', 'person'];
        concreteClient = {}; // tipo de cliente (empresa o persona)
        ctrl.dataInfo = {};
        ctrl.client =
        { // base del cliente
            nombre : "",
            pseudonimo : "",
            direccion : "",
            telefono : "",
            movil : "",
            email : "",
            observaciones: ""
        };
        ctrl.submit = function(type)
        {
            ctrl.dataInfo.client = ctrl.client;
            ctrl.dataInfo.concreteClient = ctrl.concreteClient;
            ctrl.dataInfo.clientType = ctrl.clientType;
            Client.save(ctrl.dataInfo).$promise.then(
            function(data)
            {
                ctrl.showSucces = true;
                ctrl.message = "Cliente creado correctamente";
            },
            function(data)
            {
                ctrl.showError = true;
                ctrl.message = "No se ha podido dar de alta el cliente";
            });
            $timeout(function()
            {
                ctrl.showSucces = false;
                ctrl.showError = false;
            },1000);
        };
        ctrl.flushData = function()
        {
            ctrl.client = {};
            ctrl.concreteClient = {};
        };
    });


    app.controller('clientEditController', function(Client, $routeParams, $timeout)
    {
        var ctrl = this;
        ctrl.message = "Cliente editado correctamente";
        ctrl.idClient = $routeParams.id;
        ctrl.showSucces = false;
        ctrl.showError = false;
        ctrl.clientType = "person";
        ctrl.client = {};
        ctrl.concreteClient = {};
        ctrl.dataInfo = {};

        Client.get({id:ctrl.idClient}).$promise.then(
            function(data)
            {
                ctrl.clientType = data.clientType;
                ctrl.client = data.client;
                if(ctrl.clientType === 'person')
                {
                    ctrl.concreteClient = ctrl.client.persona;
                    ctrl.client.persona = null;
                }
                else
                {
                    ctrl.concreteClient = ctrl.client.company;
                    ctrl.client.company = null;
                }
            },
            function(data)
            {
                ctrl.showError = true;
                ctrl.message = "Problema al cargar el cliente seleccionado";
            });

        ctrl.submit = function()
        {
            ctrl.dataInfo.client = ctrl.client;
            ctrl.dataInfo.concreteClient = ctrl.concreteClient;
            ctrl.dataInfo.clientType = ctrl.clientType;
            console.log(ctrl);
            Client.update({id:ctrl.idClient},ctrl.dataInfo).$promise.then(
            function(data)
            {
                ctrl.showSucces = true;
                ctrl.message = "Cliente actualizado correctamente";
            },
            function(data)
            {
                ctrl.showError = true;
                ctrl.message = "No se ha podido actualizar la informacion";
            });

            var popUp = $timeout(function()
            {
                ctrl.showSucces = false;
                ctrl.showError = false;
            },3000);


        }

    });

})();