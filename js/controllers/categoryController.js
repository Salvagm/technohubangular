(function () {

 var app = angular.module('backend'); // recoge un modulo ya creado

	app.config(['$routeProvider', // nombre real
	function($routeProvider)  // variable que utilizamos dentro de la funcion
	{
		$routeProvider.when('/categories',
		{
			templateUrl: "js/templates/categories/list.html",
			controller: 'categoryGetAllController',
			controllerAs: 'categoryGetAllCtrl'
		})
		.when('/categories/new', {
			templateUrl: 'js/templates/categories/new.html',
			controller: 'categoryCreateController',
			controllerAs: 'categoryCreateCtrl'
		})
		.when('/category/show/:id', {
			templateUrl: 'js/templates/categories/show.html',
			controller: 'categoryGetIdController',
			controllerAs: 'categoryGetIdCtrl'
		})
		.when('/category/edit/:id', {
			templateUrl: 'js/templates/categories/edit.html',
			controller: 'categoryEditController',
			controllerAs: 'categoryEditCtrl'
		});
		
	}]);

	app.controller('categoryGetAllController', function (Category, $timeout)
    {
		var categoriesCtrl = this;
		categoriesCtrl.itemsPerPage = 20;
        categoriesCtrl.rowCollection = [];
        categoriesCtrl.displayedCollection = [];
		categoriesCtrl.isLoading = true;
		categoriesCtrl.isDeleted = false;
		categoriesCtrl.message = "Categoria borrada correctamente";


		Category.get().$promise.then(
			function(data) // success
			{
				categoriesCtrl.isLoading = false;
                if(data.categories !== undefined)
			 	    categoriesCtrl.rowCollection = data.categories;
                ;
                categoriesCtrl.displayedCollection = [].concat(categoriesCtrl.rowCollection);
			},
			function(data) //error
			{
				categoriesCtrl.isLoading = false;
			});

		categoriesCtrl.remove = function(rowCat,idCat)
		{
            confirmation = confirm("Desea eliminar la categoria " + rowCat.nombre);
            var index = categoriesCtrl.rowCollection.indexOf(rowCat);
            if(confirmation === true)
            {
                Category.delete({id:idCat}).$promise.then(
                    function(data) //success
                    {
                        categoriesCtrl.isDeleted = true;
                        if(index !== -1)
                        {
                            categoriesCtrl.rowCollection.slice(index,1);
                        }

                        $timeout(function()
                        {
                            categoriesCtrl.isDeleted = false;
                        },2000);
                    },
                    function(data) //error
                    {
                        categoriesCtrl.isDeleted = false;
                    });
            }
		};
	});

	app.controller('categoryGetIdController', function ($routeParams,Category)
    {
		var categoryCtrl = this;

		categoryCtrl.title = "Categoria: ";
		categoryCtrl.name ="";
		categoryCtrl.message = "Categoria no encontrada";
		categoryCtrl.idCategory = $routeParams.id;
		categoryCtrl.families = '';
		categoryCtrl.myFamily = '';
        categoryCtrl.itemsPerPage = 20;
        categoryCtrl.rowCollection = [];
        categoryCtrl.displayedCollection = [];

        categoryCtrl.isLoading = true;
        categoryCtrl.isDeleted = false;

        categoryCtrl.category =
		{
			nombre: "",
			nomFamilia : '',
			idFamilia: '',
            products : ''
		};


		Category.get({id:categoryCtrl.idCategory}).$promise.then(function(data)
		{
			categoryCtrl.name = data.category.nombre;
            categoryCtrl.title += categoryCtrl.name;
			categoryCtrl.category.nombre = data.category.nombre;
			categoryCtrl.category.nomFamilia = data.category.familia.nombre;
			categoryCtrl.category.idFamilia = data.category.familia_id;
            categoryCtrl.category.productos = data.category.productos;
            categoryCtrl.rowCollection = data.category.productos;
            categoryCtrl.displayedCollection = [].concat(categoryCtrl.rowCollection);
            categoryCtrl.isLoading = false;

		});

    });

    app.controller('categoryEditController', function(Category,Family,$timeout,$routeParams)
    {
        var categoryEditCtrl = this;

        categoryEditCtrl.title = "Editar Categoria";
        categoryEditCtrl.name ="";
        categoryEditCtrl.message = "Categoria no encontrada";
        categoryEditCtrl.idCategory = $routeParams.id;
        categoryEditCtrl.families = [];
        categoryEditCtrl.myFamily = "";
        categoryEditCtrl.category =
        {
            nombre: "",
            nomFamilia : '',
            idFamilia: ''
        };

        Category.get({id:categoryEditCtrl.idCategory}).$promise.then(function(data)
        {
            categoryEditCtrl.name = data.category.nombre;
            categoryEditCtrl.category.nombre = data.category.nombre;
            categoryEditCtrl.category.nomFamilia = data.category.familia.nombre;
            categoryEditCtrl.category.idFamilia = data.category.familia_id;
            Family.get().$promise.then(function(data)
                {
                    categoryEditCtrl.families = data.families;
                    angular.forEach(categoryEditCtrl.families, function(value,key)
                    {
                        if(value.id===categoryEditCtrl.category.idFamilia)
                            categoryEditCtrl.myFamily = value;
                    });

                },
                function()
                {
                    categoryEditCtrl.showError = true;
                    categoryEditCtrl.message = "No se han podido cargar familias";
                    $timeout(function()
                    {
                        categoryEditCtrl.showSucces = false;
                        categoryEditCtrl.showError = false;
                    },800);
                });
        });



        categoryEditCtrl.submit = function ()
        {
            categoryEditCtrl.category.idFamilia = categoryEditCtrl.myFamily.id;

            Category.update({id:categoryEditCtrl.idCategory}, categoryEditCtrl.category).$promise.then(
                function(data) // success
                {
                    categoryEditCtrl.showSucces = true;
                    categoryEditCtrl.showError = false;
                },
                function(data) // error
                {
                    // droga para recoger el mensaje que hemos recibido
                    var key = data[Object.keys(data)[0]][Object.keys(data[Object.keys(data)[0]])][0];

                    categoryEditCtrl.message = "Error al editar Categoria: " + key;
                    categoryEditCtrl.showSucces = false;
                    categoryEditCtrl.showError = true;
                });
        };

    });

	app.controller('categoryCreateController', function (Family,Category, $timeout)
    {
		var categoryNewCtrl = this;
		categoryNewCtrl.title = "Alta Categoria";
		categoryNewCtrl.message = "Categoria guardada correctamente";
		categoryNewCtrl.showSucces = false;
        categoryNewCtrl.showError = false;
        categoryNewCtrl.families;
		categoryNewCtrl.myFamily;
		categoryNewCtrl.category = 
		{
			nombre : "",
            idFamilia : ''
		};
		categoryNewCtrl.oriCategory = angular.copy(categoryNewCtrl.category);
		Family.get().$promise.then(
			function(data){
				categoryNewCtrl.families = data.families;
				categoryNewCtrl.myFamily = data.families[0];
			},
			function()
			{
				categoryNewCtrl.showError = true;
				categoryNewCtrl.message = "No se han podido cargar familias";
				$timeout(function()
				{
					categoryNewCtrl.showSucces = false;
					categoryNewCtrl.showError = false;
				},800);
			});



		categoryNewCtrl.submit = function()
		{
			categoryNewCtrl.category.idFamilia = categoryNewCtrl.myFamily.id;
			Category.save(categoryNewCtrl.category).$promise.then(
			function(families) // success
			{
		
				categoryNewCtrl.category =  angular.copy(categoryNewCtrl.oriCategory);
				categoryNewCtrl.showSucces = true;
				categoryNewCtrl.showError = false;				
			}, 
			function(families) // error
			{
				var key = families[Object.keys(families)[0]][Object.keys(families[Object.keys(families)[0]])][0];
				categoryNewCtrl.showSucces = false;
				categoryNewCtrl.showError = true;	
				categoryNewCtrl.message = "Error al guardar categoria: " + key;	
				
			});

			$timeout(function()
			{
				categoryNewCtrl.showSucces = false;
				categoryNewCtrl.showError = false;
			},800);
			
		};
	});


})();