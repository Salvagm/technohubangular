/**
 * Created by salva on 13/08/15.
 */
(function ()
{
    var app = angular.module('backend'); // recoge un modulo ya creado

    app.config(['$routeProvider',
    function($routeProvider)
    {
        $routeProvider.when('/supliers',
        {
            templateUrl: "js/templates/supliers/list.html",
            controller: 'suplierGetAllController',
            controllerAs: 'suplierGetAllCtrl'
        })
        .when('/supliers/new', {
            templateUrl: 'js/templates/supliers/new.html',
            controller: 'suplierCreateController',
            controllerAs: 'suplierCreateCtrl'
        })
        .when('/suplier/show/:id', {
            templateUrl: 'js/templates/supliers/show.html',
            controller: 'suplierGetIdController',
            controllerAs: 'suplierGetIdCtrl'
        })
        .when('/suplier/edit/:id', {
            templateUrl: 'js/templates/supliers/edit.html',
            controller: 'suplierEditController',
            controllerAs: 'suplierEditCtrl'
        });
    }]);

    app.controller('suplierGetAllController',function(Suplier, $timeout)
    {
        var supliersCtrl = this;
        supliersCtrl.itemsPerPage = 20;
        supliersCtrl.rowCollection = [];
        supliersCtrl.displayedCollection = [];
        supliersCtrl.isLoading = true;
        supliersCtrl.isDeleted = false;
        supliersCtrl.message = "Proveedor borrado correctamente";



        Suplier.get().$promise.then(
        function(data)
        {
            supliersCtrl.isLoading = false;
            if(data.supliers !== undefined)
            {
                supliersCtrl.rowCollection = data.supliers;

            }
            supliersCtrl.displayedCollection = [].concat(supliersCtrl.rowCollection);
            console.log(supliersCtrl);
        },
        function(data)
        {
            supliersCtrl.isLoading = true;

        });

        supliersCtrl.remove = function(rowSuplier, idSuplier)
        {
            var confirmation = confirm("Desea eliminar el proveedor " + rowSuplier.nombre);
            var index = supliersCtrl.rowCollection.indexOf(rowSuplier);
            if(confirmation === true)
            {
                Suplier.delete({id:idSuplier}).$promise.then(
                    function(data) //success
                    {
                        supliersCtrl.isDeleted = true;
                        if(index !== -1)
                        {
                            supliersCtrl.rowCollection.splice(index,1);
                        }

                        $timeout(function()
                        {
                            supliersCtrl.isDeleted = false;
                        },2000);
                    },
                    function(data) //error
                    {
                        supliersCtrl.isDeleted = false;
                    });
            }
        };
    });

    app.controller('suplierGetIdController',function(Suplier, $routeParams, $location)
    {
        var suplierCtrl = this;
        suplierCtrl.message = "Producto no encontrado";
        suplierCtrl.idSuplier = $routeParams.id;
        suplierCtrl.name ="";
        suplierCtrl.isLoading = true;
        suplierCtrl.itemsPerPage = 20;
        suplierCtrl.rowCollection = [];
        suplierCtrl.displayedCollection = [];
        suplierCtrl.suplier ={};

        Suplier.get({id:suplierCtrl.idSuplier}).$promise.then(
        function(data)
        {
            suplierCtrl.isLoading = false;
            suplierCtrl.suplier = data.suplier;
            suplierCtrl.name = suplierCtrl.suplier.nombre;
            suplierCtrl.rowCollection = data.suplier.productos;
            suplierCtrl.displayedCollection = [].concat(suplierCtrl.rowCollection);
            console.log(data);

        },
        function(data)
        {

        });

    });
    app.controller('suplierCreateController',function(Suplier, $timeout)
    {
        var suplierNewCtrl = this;
        suplierNewCtrl.message = "Producto guardado correctamente";
        suplierNewCtrl.showSucces = false;
        suplierNewCtrl.showError = false;
        suplierNewCtrl.suplier =
        {
            name : "",
            valoration : 0,
            location : "",
            notes : "",
            phones : [""],
            emails: [""]

        };
        suplierNewCtrl.oriSuplier = angular.copy(suplierNewCtrl.suplier);

        suplierNewCtrl.addPhone = function()
        {
            suplierNewCtrl.suplier.phones.push('');
        };
        suplierNewCtrl.delPhone = function(index)
        {
            if(suplierNewCtrl.suplier.phones.length > 1)
                suplierNewCtrl.suplier.phones.splice(index,1);
        };
        suplierNewCtrl.addEmail = function()
        {
            suplierNewCtrl.suplier.emails.push('');
        };
        suplierNewCtrl.delEmail = function(index)
        {
            if(suplierNewCtrl.suplier.emails.length > 1)
                suplierNewCtrl.suplier.emails.splice(index,1);
        };
        suplierNewCtrl.submit = function()
        {
            if(suplierNewCtrl.suplier.valoration > 9)
                suplierNewCtrl.suplier.valoration = 9;
            else if(suplierNewCtrl.suplier.valoration < 0)
                suplierNewCtrl.suplier.valoration = 0;

            Suplier.save(suplierNewCtrl.suplier).$promise.then(
            function (reponse) {
                suplierNewCtrl.suplier =  suplierNewCtrl.oriSuplier;

                suplierNewCtrl.message = "Producto guardado correctamente";
                suplierNewCtrl.showSucces = true;
                suplierNewCtrl.showError = false;
            },
            function (response) {
                console.log(response);
                var key = "Operacion no permitida";
                if(response.status !== 500)
                {
                    key = response[Object.keys(response)[0]][Object.keys(response[Object.keys(response)[0]])][0];
                }
                suplierNewCtrl.message = "Error al guardar producto: ";
                suplierNewCtrl.message += key;
                suplierNewCtrl.showSucces = false;
                suplierNewCtrl.showError = true;
            });

        }

    });
    app.controller('suplierEditController',function(Suplier, Email, Phone, $routeParams, $timeout)
    {
        var suplierEditCtrl = this;
        suplierEditCtrl.message = "Proveedor editado correctamente";
        suplierEditCtrl.name = "";
        suplierEditCtrl.suplierId = $routeParams.id;
        suplierEditCtrl.showSucces = false;
        suplierEditCtrl.showError = false;
        suplierEditCtrl.emails = [];
        suplierEditCtrl.phones = [];
        suplierEditCtrl.promiseOutErrMsg = -1;
        suplierEditCtrl.suplier =
        {
            name : '',
            location : '',
            valoration : '',
            notes : '',
            newEmails : [],
            newPhones : [],
            updatedEmails : [],
            updatedPhones : [],
            deletedPhones : [],
            deletedEmails : [],
            products : []

        };

        Suplier.get({id : suplierEditCtrl.suplierId}).$promise.then(
        function(data)
        {
            suplierEditCtrl.name = data.suplier.nombre;
            suplierEditCtrl.suplier.name = data.suplier.nombre;
            suplierEditCtrl.suplier.location = data.suplier.direccion;
            suplierEditCtrl.suplier.valoration = parseInt(data.suplier.valoracion);
            suplierEditCtrl.suplier.notes = data.suplier.notas;
            suplierEditCtrl.suplier.products = data.suplier.productos;
            suplierEditCtrl.phones = data.suplier.telefonos;
            suplierEditCtrl.emails = data.suplier.emails;

        },
        function(data)
        {

        });

        suplierEditCtrl.addPhone = function()
        {
            suplierEditCtrl.phones.push('');
        };
        suplierEditCtrl.delPhone = function(index)
        {
            var phoneSup;
            if(suplierEditCtrl.phones.length > 1)
            {
                phoneSup = suplierEditCtrl.phones.splice(index,1);
                if(phoneSup[0].id != undefined)
                    suplierEditCtrl.suplier.deletedPhones.push(phoneSup[0].id);
            }
            else
            {
                suplierEditCtrl.message = "No puede quedarse sin telefono";;
                suplierEditCtrl.showError = true;
                if(suplierEditCtrl.timeOutMsg !== -1)
                    $timeout.cancel(suplierEditCtrl.promiseOutErrMsg);

                suplierEditCtrl.promiseOutErrMsg=  $timeout(function()
                {
                    suplierEditCtrl.showError = false;

                },2000);
            }
        };
        suplierEditCtrl.addEmail = function()
        {
            suplierEditCtrl.emails.push('');
        };
        suplierEditCtrl.delEmail = function(index)
        {
            var emailSup;
            if(suplierEditCtrl.emails.length > 1)
            {
                emailSup = suplierEditCtrl.emails.splice(index,1);
                if(emailSup[0].id != undefined)
                    suplierEditCtrl.suplier.deletedEmails.push(emailSup[0].id);
            }
            else
            {
                suplierEditCtrl.message = "No puede quedarse sin email";;
                suplierEditCtrl.showError = true;
                if(suplierEditCtrl.timeOutMsg !== -1)
                    $timeout.cancel(suplierEditCtrl.promiseOutErrMsg);

                suplierEditCtrl.promiseOutErrMsg=  $timeout(function()
                {
                    suplierEditCtrl.showError = false;

                },2000);
            }
        };
        
        suplierEditCtrl.submit = function () {
            suplierEditCtrl.emails.forEach(function(item)
            {
                if(item.id !== undefined)
                    suplierEditCtrl.suplier.updatedEmails.push(item);
                else
                    suplierEditCtrl.suplier.newEmails.push(item);
            });
            suplierEditCtrl.phones.forEach(function(item)
            {
                if(item.id !== undefined)
                    suplierEditCtrl.suplier.updatedPhones.push(item);
                else
                    suplierEditCtrl.suplier.newPhones.push(item);
            });

            console.log(suplierEditCtrl.suplier);
            Suplier.update({id : suplierEditCtrl.suplierId}, suplierEditCtrl.suplier).$promise.then(
            function (data) {

            },
            function (data) {

            });
        }
        
    });

})();