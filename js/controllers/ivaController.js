/**
 * Created by salva on 9/09/15.
 */

(function () {
    var app = angular.module('backend'); // recoge un modulo ya creado

    app.config(['$routeProvider',
    function($routeProvider)
    {
        $routeProvider.when('/options/iva',
        {
            templateUrl: "js/templates/iva/list.html",
            controller: 'ivaGetAllController',
            controllerAs: 'ivaGetAllCtrl'
        });
    }]);

    app.controller('ivaGetAllController',function(Iva, $timeout){
        ivaCtrl = this;
        ivaCtrl.isLoading = true;
        ivaCtrl.rowCollection = [];
        ivaCtrl.displayedCollection = [];
        ivaCtrl.itemPerPage = 10;
        ivaCtrl.message = " modificado correctamente";
        ivaCtrl.showSucces = false;
        ivaCtrl.showError = false;
        ivaCtrl.currentTimeOut = -1;
        ivaCtrl.hideInfo = function()
        {
            ivaCtrl.showSucces = false;
            ivaCtrl.showError = false;
        };

        Iva.get().$promise.then(
        function (data) {
            if(data.iva !== undefined)
                ivaCtrl.rowCollection = data.iva;
            ivaCtrl.displayedCollection = [].concat(ivaCtrl.rowCollection);
            ivaCtrl.displayedCollection[3].tipos_iva = "RECARGO DE EQUIVALENCIA";
            ivaCtrl.isLoading = false;
        },
        function (data) {

        });
        ivaCtrl.checkPercentaje = function (value)
        {
          if(value < 0)
          {
              ivaCtrl.showError = true;
              ivaCtrl.message = "Valor incorrecto";
              $timeout.cancel(ivaCtrl.currentTimeOut);
              ivaCtrl.currentTimeOut = $timeout(ivaCtrl.hideInfo,2000);
              return false;
          }
        };
        ivaCtrl.modify = function (idIva,newValue) {
            Iva.update({id:idIva},newValue).$promise.then(
            function (data) {
                ivaCtrl.showSucces = true;
                ivaCtrl.message = "IVA " + data.iva.tipos_iva.toLowerCase() + " modificado correctamente";
                $timeout.cancel(ivaCtrl.currentTimeOut);
                ivaCtrl.currentTimeOut = $timeout(ivaCtrl.hideInfo,2000);
                console.log(ivaCtrl);
            },
            function (data) {
                ivaCtrl.showError = true;
                ivaCtrl.message = data.data.error;
                $timeout.cancel(ivaCtrl.currentTimeOut);
                ivaCtrl.currentTimeOut = $timeout(ivaCtrl.hideInfo,2000);
                console.log(ivaCtrl);
            });
        };
    });

})();