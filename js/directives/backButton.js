/**
 * Created by salva on 11/08/15.
 */
(function() {
    var module = angular.module('backend.directives');

    module.directive('backButton',function()
    {
        // Devolvemos el objeto que contiene la config de la directiva
        return {
            restrict : 'A',
            templateUrl : 'js/directives/backButton.html',
            controller : 'dirBackButtonController',
            controllerAs : 'dirBackBtnCtrl'
        };
    });

    module.controller('dirBackButtonController', function($window)
    {
        this.backClick = function()
        {
            $window.history.back();
        }
    });
})();