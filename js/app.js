(function () { // encapsula ambito


    angular.module('backend.directives',[]); // creamos un nuevo modulo para directivas "nameSpace"

    var app = angular.module('backend', ['ngRoute',
        'xeditable',
        'smart-table',
        'ngResource',
        'rzModule',
        'backend.directives']); // crea un nuevo modulo
    // Constantes utilizadas en la app
    app.constant('BaseConf',{
        "API_URL" : "http://technohub.com/api"
    });



    app.config(['$routeProvider',
        function($routeProvider)
        {
            $routeProvider.when('/', {
                templateUrl: 'js/templates/base.html',
                controller: 'homeController',
                controllerAs: 'homeCtrl'
            })
            .otherwise({ redirectTo: '/' });
        }]);

    app.controller('homeController', ['$scope', function ($scope) {

    }]);

})();
