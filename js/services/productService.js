(function () {

    var app = angular.module('backend');

    app.factory('Product', function ($resource, BaseConf) {

        return $resource(BaseConf.API_URL + "/products/:id", {id: "@_id"},
            {
                update : {method : "PUT", params: {id: "@id"}},
            });
     });

})();