/**
 * Created by salva on 8/09/15.
 */

(function () {

    var app = angular.module('backend');

    app.factory('Iva', function ($resource, BaseConf) {

        return $resource(BaseConf.API_URL + "/iva/:id", {id: "@_id"},
        {
            update : {method : "PUT" , params : {id:"@id"}},
        });
    });

})();