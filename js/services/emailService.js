/**
 * Created by salva on 13/08/15.
 */
(function () {

    var app = angular.module('backend');

    app.factory('Email', function ($resource, BaseConf) {

        return $resource(BaseConf.API_URL + "/emails/:id", {id: "@_id"},
            {
                update : {method : "PUT", params: {id: "@id"}},
            });
    });

})();