(function () {

    var app = angular.module('backend');

    app.factory('Category', function ($resource, BaseConf) {

        return $resource(BaseConf.API_URL + "/categories/:id", {id: "@_id"},
            {
                update : {method : "PUT", params: {id: "@id"}},
            });
     });

})();