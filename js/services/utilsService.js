/**
 * Created by salva on 8/09/15.
 */

(function () {
    var app = angular.module('backend');
    app.service('Utils', function () {
        var utils = this;
        /**
         * Funcion para marcar como seleccionados los item recibidos.
         * Esta funcion realiza sus operaciones cuando recibimos un objeto de copia
         * @param itemListTemp lista temporal recibida desde el servidor
         * @param itemList lista interna que hemos recibido de la copia
         * @returns {Array} Lista actualizada
         */
        utils.checkListOfItems = function(itemListTemp, itemList)
        {
            var index = 0;
            var indexTemp = 0;
            var updated = false;

            while(index < itemList.length)
            {
                while(!updated)
                {
                    if(itemListTemp[indexTemp].id === itemList[index])
                    {
                        itemListTemp[indexTemp]._isSelect = true;
                        updated = true;
                    }
                    ++indexTemp;
                }
                indexTemp = 0;
                updated = false;
                ++index;
            }
            return itemListTemp;
        };
    })
})();