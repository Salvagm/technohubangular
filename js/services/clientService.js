/**
 * Created by salva on 26/10/15.
 */


(function () {

    var app = angular.module('backend');

    app.factory('Person', function ($resource, BaseConf) {

        return $resource(BaseConf.API_URL + "/clients/persons/:id", {id: "@_id"},
            {
                update : {method : "PUT", params: {id: "@id"}}
            });
    });

    app.factory('Company', function ($resource, BaseConf) {
      return $resource(BaseConf.API_URL + "/clients/companies/:id", {id : "@_id"},
          {
              update : {method : "PUT", params: {id: "@id"}}
          });
    });

    app.factory('Client', function($resource,BaseConf)
    {
       return $resource(BaseConf.API_URL + "/clients/:id:type", {id: "@_id", type: "@_type"},
           {
               update : {method : "PUT", params : {id : "@id"}},
               //all : {method : "GET", params : {type : "@type"}}
           });
    });

})();