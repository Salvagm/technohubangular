(function () {

    var app = angular.module('backend');

    app.factory('Family', function ($resource, BaseConf) {

        return $resource(BaseConf.API_URL + "/families/:id", {id: "@_id"},
            {
                update : {method : "PUT", params: {id: "@id"}},
            });
     });

})();